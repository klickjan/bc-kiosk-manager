# Kiosk manager
This project deals with managing kiosks (computer showing a web page). Part of the project is server (see server/readme.md) and client with quick installation process for Raspberry pi (see client/readme.md).

Active version of this repository can be found at https://gitlab.com/klicka-jan/kiosk-manager