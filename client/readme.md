# Client 
This kiosk client is meant for Raspberry Pi OS.

This installation is meant to be simple for administrator (run one script). The RPi will connect itself to the server after running. This installation process is supported for Linux devices only. The script mounts the data into /mnt/kiosk-manager/{current millis}/sd-boot and /mnt/kiosk-manager/{current millis}/sd-data.

## Setup
- download and unzip Raspberry Pi OS Desktop version to a file named raspbian.os into the same folder as prepare-rpi.os from https://www.raspberrypi.org/software/operating-systems/
- sd-files/wpa_supplicant.conf - fill in WiFi connection for the device
- sd-files/server_hostname - fill in hostname or IP of the running server
- sd-files/ssh/authorized_keys - optional - fill in with authorized keys to be copied to Raspberry Pi for SSH connection
- it's advised to turn off automounting of sd cards or to edit the script not to mount the flashed sd card as this may lead to SD data corruption

## Installation
- figure out which device is the sd card mounted as - eg. `sudo fdisk -l`
- run prepare-rpi.sh as sudo
  - parameters
    1) alfanumeric kiosk name
    2) sd card device (eg /dev/sda)
    3) optional - URL of web page to be shown (will be saved on server during registration), can be filled on server after registration.
- run rpi

