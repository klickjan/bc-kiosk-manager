# Server
Server for managing kiosks. Chaging URL is done by changing the entity kiosk. Kiosks periodically connect to the server and download current URL and any additional actions to be performed. For REST API see Controller classes documentation in the package api.

## Setup

1. Database
	- use Docker (docker-compose.yml)
	- use Postgres manually
    	- create DB with 00_create_db.sql
    	- create DM with 01_create_model.sql
    - configure applications.properties with access to db
2. If using Intellij, download plugin Ebean enhancer, enable it in Build menu
    - check that Ebean generates Query beans (https://ebean.io/docs/getting-started/intellij-idea)
3. Run
4. ??? Profit

## Deployment

### Docker
- publish new Docker image klickjan/kiosk-manager
- use docker-compose.yml to run app

### Manual
- Install Postgres
	- create DB with 00_create_db.sql
	- create DM with 01_create_model.sql
- Install server
	- build .jar
	- edit application.properties with access to Postgres
	- run



