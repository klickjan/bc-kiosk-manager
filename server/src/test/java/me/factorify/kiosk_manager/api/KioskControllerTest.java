package me.factorify.kiosk_manager.api;

import me.factorify.kiosk_manager.api.exception.ControllerExceptionHandler;
import me.factorify.kiosk_manager.application_logic.KioskService;
import me.factorify.kiosk_manager.data.entity.ClientConnection;
import me.factorify.kiosk_manager.data.entity.Kiosk;
import me.factorify.kiosk_manager.data.repository.ClientConnectionsRepository;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static me.factorify.kiosk_manager.TestUtils.toJson;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

class KioskControllerTest {
    private KioskService kioskService = Mockito.mock(KioskService.class);
    private ClientConnectionsRepository clientConnectionsRepository = Mockito.mock(ClientConnectionsRepository.class);

    private MockMvc mockMvc = MockMvcBuilders
            .standaloneSetup(new KioskController(kioskService, clientConnectionsRepository))
            .setControllerAdvice(new ControllerExceptionHandler())
            .build();

    private final Kiosk a = new Kiosk(1L, "a", "www.google.com", null, null);
    private final Kiosk b = new Kiosk(2L, "b", null, null, null);
    private final List<Kiosk> kiosks = List.of(a, b);
    private final Kiosk c = new Kiosk("c", null, null, null);

    @Test
    void getAll() throws Exception {
        BDDMockito.given(kioskService.getAll()).willReturn(kiosks);
        mockMvc.perform(
                MockMvcRequestBuilders.get("/kiosk")
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.[0].name", CoreMatchers.is("a")))
                .andExpect(jsonPath("$.[0].url", CoreMatchers.is("www.google.com")))
                .andExpect(jsonPath("$.[0].location", CoreMatchers.nullValue()))
                .andExpect(jsonPath("$.[0].description", CoreMatchers.nullValue()))

                .andExpect(jsonPath("$.[1].name", CoreMatchers.is("b")))
                .andExpect(jsonPath("$.[1].url", CoreMatchers.nullValue()));
    }

    @Test
    void getById() throws Exception {
        BDDMockito.given(kioskService.getByIdOrThrow(1L)).willReturn(a);
        mockMvc.perform(
                MockMvcRequestBuilders.get("/kiosk/1")
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.name", CoreMatchers.is("a")))
                .andExpect(jsonPath("$.url", CoreMatchers.is("www.google.com")))
                .andExpect(jsonPath("$.location", CoreMatchers.nullValue()))
                .andExpect(jsonPath("$.description", CoreMatchers.nullValue()));
    }

    @Test
    void getByIdFails() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.get("/kiosk/aaa")
        ).andExpect(MockMvcResultMatchers.status().isBadRequest());

        BDDMockito.given(kioskService.getByIdOrThrow(2221)).willThrow(EntityNotFoundException.class);
        mockMvc.perform(
                MockMvcRequestBuilders.get("/kiosk/2221")
        ).andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    void create() throws Exception {
        BDDMockito.given(kioskService.getByName("c")).willReturn(Optional.empty());
        BDDMockito.given(kioskService.save(ArgumentMatchers.any(Kiosk.class))).willReturn(c);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/kiosk")
                        .content(toJson(c))
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(jsonPath("$.name", CoreMatchers.is("c")))
                .andExpect(jsonPath("$.url", CoreMatchers.nullValue()))
                .andExpect(jsonPath("$.location", CoreMatchers.nullValue()))
                .andExpect(jsonPath("$.description", CoreMatchers.nullValue()));
    }

    @Test
    void createWithIdFails() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/kiosk")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(a))
        ).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void createExisting() throws Exception {
        BDDMockito.given(kioskService.getByName("c")).willReturn(Optional.of(c));
        mockMvc.perform(
                MockMvcRequestBuilders.post("/kiosk")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(c))
        ).andExpect(MockMvcResultMatchers.status().isConflict());
    }

    @Test
    void update() throws Exception {
        var aCopy = new Kiosk(null, null, null, null).copyValuesFrom(a);
        aCopy.setId(1L);
        var aCopyEdited = new Kiosk(null, null, null, null).copyValuesFrom(a);
        aCopyEdited.setId(1L);
        aCopyEdited.setUrl("www.new.url");

        BDDMockito.given(kioskService.getById(1L)).willReturn(Optional.of(aCopy));
        BDDMockito.given(kioskService.save(aCopyEdited)).willReturn(aCopyEdited);


        mockMvc.perform(
                MockMvcRequestBuilders.put("/kiosk/1")
                        .content(toJson(aCopyEdited))
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.name", CoreMatchers.is("a")))
                .andExpect(jsonPath("$.url", CoreMatchers.is("www.new.url")))
                .andExpect(jsonPath("$.location", CoreMatchers.nullValue()))
                .andExpect(jsonPath("$.description", CoreMatchers.nullValue()));

    }

    @Test
    void updateNotFound() throws Exception {
        BDDMockito.given(kioskService.getById(-1L)).willReturn(Optional.empty());
        mockMvc.perform(
                MockMvcRequestBuilders.put("/kiosk/-1")
                        .content(toJson(a))
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void delete() throws Exception {
        BDDMockito.when(kioskService.getByIdOrThrow(1)).thenReturn(a);
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/kiosk/1")
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void deleteNotFound() throws Exception {
        BDDMockito.when(kioskService.getByIdOrThrow(-1)).thenThrow(EntityNotFoundException.class);
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/kiosk/-1")
        ).andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void statsNotFound() throws Exception {
        BDDMockito.when(kioskService.getByIdOrThrow(-1)).thenThrow(EntityNotFoundException.class);
        mockMvc.perform(
                MockMvcRequestBuilders.get("/kiosk/-1/stats")
        ).andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void stats() throws Exception {
        BDDMockito.when(kioskService.getByIdOrThrow(1)).thenReturn(a);
        BDDMockito.when(clientConnectionsRepository.findByKioskId(1)).thenReturn(List.of(new ClientConnection(a, "a.a.a", LocalDateTime.of(2021, 5, 1, 19, 0))));
        mockMvc.perform(
                MockMvcRequestBuilders.get("/kiosk/1/stats")
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.kiosk.name", CoreMatchers.is("a")))
                .andExpect(jsonPath("$.connections.[0].ipAddress", CoreMatchers.is("a.a.a")));
    }

    @Test
    void statsAll() throws Exception {
        BDDMockito.when(kioskService.getAll()).thenReturn(List.of(a, b));
        BDDMockito.when(clientConnectionsRepository.findByKioskId(1)).thenReturn(List.of(new ClientConnection(a, "a", LocalDateTime.of(2021, 5, 1, 19, 0))));
        BDDMockito.when(clientConnectionsRepository.findByKioskId(2)).thenReturn(List.of(new ClientConnection(b, "b", LocalDateTime.now())));
        mockMvc.perform(
                MockMvcRequestBuilders.get("/kiosk/stats")
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.[0].kiosk.name", CoreMatchers.is("a")))
                .andExpect(jsonPath("$.[0].connections.[0].ipAddress", CoreMatchers.is("a")))
                .andExpect(jsonPath("$.[0].active", CoreMatchers.is(false)))
                .andExpect(jsonPath("$.[1].kiosk.name", CoreMatchers.is("b")))
                .andExpect(jsonPath("$.[1].connections.[0].ipAddress", CoreMatchers.is("b")))
                .andExpect(jsonPath("$.[1].active", CoreMatchers.is(true)));
    }

    @Test
    void statsEmpty() throws Exception {
        BDDMockito.when(kioskService.getAll()).thenReturn(List.of());
        mockMvc.perform(
                MockMvcRequestBuilders.get("/kiosk/stats")
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().string("[]"));
    }
}
