package me.factorify.kiosk_manager.api.dto;

import me.factorify.kiosk_manager.KioskManagerTestConfig;
import me.factorify.kiosk_manager.data.entity.ClientConnection;
import me.factorify.kiosk_manager.data.entity.Kiosk;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class KioskStatisticsTest {
    Kiosk kiosk = new Kiosk("a", null, null, null);

    @Test
    void isActive() {

        var cons = List.of(
                new ClientConnection(kiosk, "", LocalDateTime.now().minusMinutes(1)),
                new ClientConnection(kiosk, "", LocalDateTime.now().minusMinutes(11)),
                new ClientConnection(kiosk, "", LocalDateTime.now().minusMinutes(21)),
                new ClientConnection(kiosk, "", LocalDateTime.now().minusMinutes(31)),
                new ClientConnection(kiosk, "", LocalDateTime.now().minusMinutes(41)),
                new ClientConnection(kiosk, "", LocalDateTime.now().minusMinutes(51)),
                new ClientConnection(kiosk, "", LocalDateTime.now().minusMinutes(61)),
                new ClientConnection(kiosk, "", LocalDateTime.now().minusMinutes(71))
        );

        var stats = KioskStatistics.build(kiosk, cons);
        assertTrue(stats.isActive());
    }


    @Test
    void isActive2() {

        var cons = List.of(
                new ClientConnection(kiosk, "", LocalDateTime.now().minusMinutes(71)),
                new ClientConnection(kiosk, "", LocalDateTime.now().minusMinutes(51))
        );

        var stats = KioskStatistics.build(kiosk, cons);
        assertTrue(stats.isActive());
    }

    @Test
    void isInactive() {
        var cons = List.of(
                new ClientConnection(kiosk, "", LocalDateTime.now().minusMinutes(71))
        );

        var stats = KioskStatistics.build(kiosk, cons);
        assertFalse(stats.isActive());
    }
}
