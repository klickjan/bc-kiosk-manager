package me.factorify.kiosk_manager.api.serializers;

import me.factorify.kiosk_manager.application_logic.client_action.ClientAction;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ClientActionTextSerializerTest {

    @Test
    void serialize() {
        assertEquals("URL;www.google.com\nSYSLOG\nRESTART",
                new ClientActionTextSerializer(List.of(
                        new ClientAction(ClientAction.Type.URL, "www.google.com"),
                        new ClientAction(ClientAction.Type.SYSLOG),
                        new ClientAction(ClientAction.Type.RESTART)
                )).serialize()
        );
    }
}
