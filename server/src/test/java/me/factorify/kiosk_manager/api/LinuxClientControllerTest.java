package me.factorify.kiosk_manager.api;

import me.factorify.kiosk_manager.KioskManagerTestConfig;
import me.factorify.kiosk_manager.api.exception.ControllerExceptionHandler;
import me.factorify.kiosk_manager.application_logic.KioskService;
import me.factorify.kiosk_manager.application_logic.client_action.ClientAction;
import me.factorify.kiosk_manager.application_logic.client_action.ClientActionService;
import me.factorify.kiosk_manager.data.entity.ClientConnection;
import me.factorify.kiosk_manager.data.entity.Kiosk;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


class LinuxClientControllerTest extends KioskManagerTestConfig {
    @MockBean
    private ClientActionService clientActionService;

    @MockBean
    private KioskService kioskService;


    @Autowired
    private MockMvc mockMvc;

    @Test
    void getClientVersion() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.get("/client/linux/version")
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    void registerKiosk() throws Exception {
        var abc = new Kiosk("abc", "www.google.com", null, null);
        BDDMockito.given(clientActionService.register("abc", "www.google.com")).willReturn(abc);
        mockMvc.perform(
                MockMvcRequestBuilders.put("/client/linux/register/abc")
                        .content("{\"url\" : \"www.google.com\"}")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.name", CoreMatchers.is("abc")))
                .andExpect(jsonPath("$.url", CoreMatchers.is("www.google.com")))
                .andExpect(jsonPath("$.location", CoreMatchers.nullValue()))
                .andExpect(jsonPath("$.description", CoreMatchers.nullValue()));
    }

    @Test
    void pendingActions() throws Exception {
        var abc = new Kiosk("abc", "www.google.com", null, null);
        BDDMockito.given(kioskService.getByNameOrThrow("abc")).willReturn(abc);
        BDDMockito.given(clientActionService.heartbeat(abc, null)).willReturn(new ClientConnection(abc, "", LocalDateTime.now()));
        BDDMockito.given(clientActionService.processActions(abc)).willReturn(List.of(
                new ClientAction(ClientAction.Type.URL, "www.google.com"),
                new ClientAction(ClientAction.Type.RESTART)
        ));

        mockMvc.perform(
                MockMvcRequestBuilders.post("/client/linux/action/abc")
                        .accept(MediaType.TEXT_PLAIN)
//                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.content().string("URL;www.google.com\nRESTART"));
    }
}
