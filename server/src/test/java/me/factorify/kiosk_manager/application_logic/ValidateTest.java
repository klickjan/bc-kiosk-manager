package me.factorify.kiosk_manager.application_logic;

import me.factorify.kiosk_manager.KioskManagerTestConfig;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidateTest {

    @Test
    void kioskNameValid() {
        assertEquals("aaa", Validate.kioskName("aaa"));
        assertEquals("aaa2323", Validate.kioskName("aaa2323"));
        assertEquals("123123", Validate.kioskName("123123"));
        assertEquals("z", Validate.kioskName("z"));
        assertEquals("1", Validate.kioskName("1"));
        assertEquals("9", Validate.kioskName("9"));
        assertEquals("9aAAAAasd", Validate.kioskName("9aAAAAasd"));
    }

    @Test
    void kioskNameInvalid() {
        assertThrows(IllegalArgumentException.class,
                () -> Validate.kioskName("hello!")
        );
        assertThrows(IllegalArgumentException.class,
                () -> Validate.kioskName("a   sdfa !")
        );
        assertThrows(IllegalArgumentException.class,
                () -> Validate.kioskName("     ")
        );
        assertThrows(IllegalArgumentException.class,
                () -> Validate.kioskName("!!!!")
        );
        assertThrows(IllegalArgumentException.class,
                () -> Validate.kioskName("$$@$#$$#$#")
        );
    }
}
