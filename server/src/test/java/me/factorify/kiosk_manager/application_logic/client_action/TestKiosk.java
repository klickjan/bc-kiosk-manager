package me.factorify.kiosk_manager.application_logic.client_action;

import me.factorify.kiosk_manager.data.entity.Kiosk;

import java.util.Objects;

/**
 * Due to the nature of Ebean implementing equals, this class acts as proxy to be able to test without database.
 * https://archive-avaje-org.github.io/equals.html
 */
public class TestKiosk extends Kiosk {
    public TestKiosk(String name, String url, String location, String description) {
        super(name, url, location, description);
    }

    @Override
    public boolean equals(Object other) {
        if (other.getClass().equals(Kiosk.class) || other.getClass().equals(TestKiosk.class)) {
            Kiosk kiosk = (Kiosk) other;
            return Objects.equals(getName(), kiosk.getName()) &&
                    Objects.equals(getUrl(), kiosk.getUrl()) &&
                    Objects.equals(getDescription(), kiosk.getDescription()) &&
                    Objects.equals(getLocation(), kiosk.getLocation());
        } else {
            return false;
        }
    }
}
