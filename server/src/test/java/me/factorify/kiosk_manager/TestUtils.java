package me.factorify.kiosk_manager;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TestUtils {
    public static String toJson(Object object) throws Exception {
        return new ObjectMapper().writeValueAsString(object);
    }
}
