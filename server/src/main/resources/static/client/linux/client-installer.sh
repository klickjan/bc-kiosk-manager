kioskWd='/home/pi/kiosk-client'
kioskDlWd="${kioskWd}/download"
kioskUlWd="${kioskWd}/upload"

domain=$(cat "${kioskWd}/server-hostname")
kioskName=$(cat "${kioskWd}/identity")

versionEndpoint='api/client/linux/version'
registerEndpoint="api/client/linux/register/${kioskName}"

# files
clientEndpoint='api/client/linux/client.sh'
systemdServiceEndpoint='api/client/linux/kiosk-client.service'
systemdTimerEndpoint='api/client/linux/kiosk-client.timer'
autostartTemplateEndpoint='api/client/linux/autostart_template'

function registerClient() {
    echo "Registering client"

    if [ -f  "${kioskDlWd}/url" ]; then
        currentUrl=$(cat "${kioskDlWd}/url")
        curl -f -m 60 -X PUT -H "Content-Type: application/json" -d "{\"url\":\"${currentUrl}\"}" "${domain}/${registerEndpoint}"
    else
        curl -f -m 60 -X PUT -H "Content-Type: application/json" -d "{}" "${domain}/${registerEndpoint}"
    fi

    if [ "$?" -ne 0 ]; then
        echo "Failed to register client."
        exit 1
    fi

}

function downloadClient() {
    echo "Downloading new client '${domain}'"

    mkdir -p "${kioskDlWd}/"
    mkdir -p "${kioskUlWd}/"

    curl -f -m 60 -G "${domain}/${clientEndpoint}" -o "${kioskDlWd}/client.sh"
    curl -f -m 60 -G "${domain}/${versionEndpoint}" -o "${kioskDlWd}/version"

    curl -f -m 60 -G "${domain}/${systemdServiceEndpoint}" -o "${kioskDlWd}/kiosk-client.service"
    curl -f -m 60 -G "${domain}/${systemdTimerEndpoint}" -o "${kioskDlWd}/kiosk-client.timer"

    curl -f -m 60 -G "${domain}/${autostartTemplateEndpoint}" -o "${kioskDlWd}/autostart_template"

    echo "Client downloaded into ${kioskDlWd}/"
}

function stopClient() {
    echo "Stopping client service"
    systemctl stop kiosk-client.timer
    systemctl stop kiosk-client.service
    sleep 1s
}

function installClient() {
    echo "Installing client files from ${kioskDlWd} to ${kioskWd}"

    mv "${kioskDlWd}/client.sh" "${kioskWd}/client.sh"
    mv "${kioskDlWd}/version" "${kioskWd}/version"
    mv "${kioskDlWd}/autostart_template" "${kioskWd}/autostart_template"

    mv "${kioskDlWd}/kiosk-client.timer" "${kioskDlWd}/kiosk-client.service" "/etc/systemd/system/"

    mkdir -p "/home/pi/.config/lxsession/LXDE-pi/"
}


function runClient() {
    echo "Reloading daemon"
    systemctl daemon-reload

    echo "Enabling client service"
    systemctl enable kiosk-client.service
    systemctl enable kiosk-client.timer

    echo "Running client service"
    systemctl start kiosk-client.service
    systemctl start kiosk-client.timer
}

registerClient
downloadClient
stopClient
installClient
runClient

echo "Installer finished"
