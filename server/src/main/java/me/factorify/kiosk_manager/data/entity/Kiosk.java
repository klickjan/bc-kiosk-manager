package me.factorify.kiosk_manager.data.entity;

import javax.persistence.*;

@Entity
@Table(name = "kiosks")
public class Kiosk {
    @Id
    private Long id;

    /**
     * See {@link me.factorify.kiosk_manager.application_logic.Validate#kioskName(String)} for valid kiosk name.
     */
    @Column(nullable = false)
    private String name;

    private String url;

    private String location;

    private String description;

    public Kiosk(String name, String url, String location, String description) {
        this.name = name;
        this.url = url;
        this.location = location;
        this.description = description;
    }

    public Kiosk(Long id, String name, String url, String location, String description) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.location = location;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Kiosk{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", location='" + location + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public Kiosk copyValuesFrom(Kiosk kiosk) {
        this.name = kiosk.name;
        this.url = kiosk.url;
        this.location = kiosk.location;
        this.description = kiosk.description;
        return this;
    }
}
