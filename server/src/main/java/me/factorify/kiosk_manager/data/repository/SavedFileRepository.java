package me.factorify.kiosk_manager.data.repository;

import me.factorify.kiosk_manager.data.entity.SavedFile;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Repository for handling CRUD operations for SavedFile.
 */
@Repository
public interface SavedFileRepository {
    Optional<SavedFile> getById(long id);

    /**
     * Gets and filters saved files based on optional parameters. If all parameters are null, all files will be returned.
     */
    List<SavedFile> getFiltered(String kioskName, SavedFile.Type type);

    SavedFile save(SavedFile savedFile);

    void delete(long id);
}
