package me.factorify.kiosk_manager.data.repository;

import me.factorify.kiosk_manager.data.entity.ClientConnection;

import java.util.List;
import java.util.Optional;

/**
 * Repository for handling CRUD operations for ClientConnection.
 */
public interface ClientConnectionsRepository {
    ClientConnection save(ClientConnection clientConnection);
    Optional<ClientConnection> findByKioskIdAndIp(long id, String ip);
    List<ClientConnection> findByKioskId(long id);
}
