package me.factorify.kiosk_manager.data.entity;

import io.ebean.annotation.DbEnumValue;
import io.ebean.annotation.EnumValue;
import io.ebean.annotation.NotNull;

import javax.persistence.*;

/**
 * Represents file that is managed by this server.
 */
@Entity
@Table(name = "saved_files")
public class SavedFile {
    @Id
    private long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String path;

    @ManyToOne
    private Kiosk kiosk;

    @Column(nullable = false)
    private Type type;

    public SavedFile(String name, String path, Kiosk kiosk, Type type) {
        this.name = name;
        this.path = path;
        this.kiosk = kiosk;
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Kiosk getKiosk() {
        return kiosk;
    }

    public void setKiosk(Kiosk kiosk) {
        this.kiosk = kiosk;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "SavedFile{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", path='" + path + '\'' +
                ", kiosk=" + (kiosk != null ? kiosk.getName() : null) +
                ", type=" + type +
                '}';
    }

    public enum Type {
        SYSTEM_LOG("SYSTEM_LOG"),
        BROWSER_LOG("BROWSER_LOG"),
        OTHER("OTHER");

        String dbValue;

        Type(String dbValue) {
            this.dbValue = dbValue;
        }

        @DbEnumValue
        public String getDbValue() {
            return dbValue;
        }
    }
}
