package me.factorify.kiosk_manager.data;

import io.ebean.Database;
import io.ebean.DatabaseFactory;
import io.ebean.config.DatabaseConfig;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

/**
 * Creates Spring bean for Ebean database access.
 */
@Component
public class EbeanDatabaseFactoryBean implements FactoryBean<Database> {
    @Override
    public Database getObject() {
        DatabaseConfig config = new DatabaseConfig();
        config.setName("db");
        config.loadFromProperties();
        return DatabaseFactory.create(config);
    }

    @Override
    public Class<?> getObjectType() {
        return Database.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
