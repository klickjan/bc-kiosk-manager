package me.factorify.kiosk_manager.application_logic.client_action;

import me.factorify.kiosk_manager.application_logic.KioskService;
import me.factorify.kiosk_manager.application_logic.Validate;
import me.factorify.kiosk_manager.data.entity.ClientConnection;
import me.factorify.kiosk_manager.data.entity.Kiosk;
import me.factorify.kiosk_manager.data.repository.ClientConnectionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Service for handling logic around kiosk client actions both for the client and the user, such as restarting, requesting logs, registering etc.
 * <p>
 * Actions are stored only in memory and no history is saved.
 */
@Service
public class ClientActionInMemoryService implements ClientActionService {

    private final KioskService kioskService;

    private final ClientConnectionsRepository clientConnectionsRepository;

    private final ConcurrentHashMap<String, List<ClientAction>> actionsByKioskName = new ConcurrentHashMap<>();

    @Autowired
    public ClientActionInMemoryService(KioskService kioskService, ClientConnectionsRepository clientConnectionsRepository) {
        this.kioskService = kioskService;
        this.clientConnectionsRepository = clientConnectionsRepository;
    }

    @Override
    public Kiosk register(String kioskName, String url) {
        Validate.kioskName(kioskName);
        return kioskService
                .getByName(kioskName)
                .orElseGet(() -> kioskService.save(new Kiosk(kioskName, url, null, null)));
    }

    @Override
    public Map<String, List<ClientAction>> getAllActions() {
        return new HashMap<>(actionsByKioskName);
    }

    @Override
    public List<ClientAction> processActions(Kiosk kiosk) {
        var actions = new ArrayList<ClientAction>();
        if (kiosk.getUrl() != null) {
            actions.add(new ClientAction(ClientAction.Type.URL, kiosk.getUrl()));
        }
        actions.addAll(removeActions(kiosk.getName()));
        return actions;
    }

    @Override
    public List<ClientAction> getActionsFor(Kiosk kiosk) {
        var actions = actionsByKioskName.get(kiosk.getName());
        if (actions == null) {
            return new ArrayList<>();
        } else {
            return new ArrayList<>(actions);
        }
    }

    @Override
    public ClientConnection heartbeat(Kiosk kiosk, String ipAddress) {
        var connection = clientConnectionsRepository
                .findByKioskIdAndIp(kiosk.getId(), ipAddress)
                .orElseGet(() -> new ClientConnection(kiosk, ipAddress, LocalDateTime.now()));
        connection.setLastConnectedAt(LocalDateTime.now());
        return clientConnectionsRepository.save(connection);
    }

    @Override
    public void addAction(Kiosk kiosk, ClientAction action) {
        actionsByKioskName.compute(kiosk.getName(), (key, value) -> {
            List<ClientAction> newList = value == null ? new ArrayList<>() : new ArrayList<>(value);
            newList.add(action);
            return newList;
        });
    }

    @Override
    public void addAction(ClientAction action) {
        this.kioskService.getAll().forEach(kiosk -> addAction(kiosk, action));
    }

    @Override
    public void addAction(List<Kiosk> kiosks, ClientAction clientAction) {
        kiosks.forEach(kiosk -> addAction(kiosk, clientAction));
    }

    /**
     * Removes all currently planned actions.
     */
    @Override
    public void clearActions() {
        this.actionsByKioskName.clear();
    }

    private List<ClientAction> removeActions(String kioskName) {
        List<ClientAction> actions = actionsByKioskName.remove(kioskName);
        return actions != null ? actions : new ArrayList<>();
    }
}
