package me.factorify.kiosk_manager.application_logic.storage;

import me.factorify.kiosk_manager.data.entity.SavedFile;
import me.factorify.kiosk_manager.data.entity.Kiosk;
import me.factorify.kiosk_manager.data.repository.SavedFileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of the {@link StorageService} for storing files locally.
 * The URL is specified by the {@code storage.path} property in application.properties file.
 */
@Service
public class LocalFileStorage implements StorageService {
    private final String storagePath;
    private final SavedFileRepository savedFileRepository;

    @Autowired
    public LocalFileStorage(@Value("${storage.path}") String storagePath,
                            SavedFileRepository savedFileRepository) {
        this.storagePath = storagePath;
        this.savedFileRepository = savedFileRepository;
    }

    @Override
    public SavedFile save(MultipartFile multipartFile, Kiosk kiosk, SavedFile.Type type) throws IOException {
        Path filePath = saveFileToDisk(multipartFile);
        SavedFile file = new SavedFile(
                filePath.getFileName().toString(),
                filePath.toAbsolutePath().toString(),
                kiosk,
                type);
        return savedFileRepository.save(file);
    }

    @Override
    public Optional<SavedFile> getById(long id) {
        return savedFileRepository.getById(id);
    }

    @Override
    public SavedFile getByIdOrThrow(long id) {
        return this.getById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public byte[] getFileContent(long id) throws IOException {
        return Files.readAllBytes(Paths.get(getByIdOrThrow(id).getPath()));
    }

    @Override
    public List<SavedFile> getFiltered(String kioskName, SavedFile.Type type) {
        return savedFileRepository.getFiltered(kioskName, type);
    }

    @Override
    public List<SavedFile> getByKioskName(String kioskName) {
        return savedFileRepository.getFiltered(kioskName, null);
    }

    @Override
    public void delete(long id) throws IOException {
        SavedFile file = getByIdOrThrow(id);
        Path filePath = Paths.get(file.getPath());
        if (filePath.toFile().exists()) {
            Files.delete(filePath);
        }
        savedFileRepository.delete(id);
    }

    private Path saveFileToDisk(MultipartFile multipartFile) throws IOException {
        Path filePath = createPath(multipartFile.getOriginalFilename());
        multipartFile.transferTo(filePath);
        return filePath;
    }

    private Path createPath(String fileName) throws IOException {
        Path filePath = Paths.get(storagePath, "" + System.currentTimeMillis(), fileName);
        File file = filePath.toFile();
        File parent = filePath.getParent().toFile();
        if (!parent.exists() && !parent.mkdirs()) {
            throw new IOException("Can't create parent" + parent.getName());
        }
        if (!file.createNewFile()) {
            throw new IOException("Can't create file " + fileName);
        }
        return filePath;
    }
}
