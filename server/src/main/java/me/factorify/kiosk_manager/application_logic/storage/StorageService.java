package me.factorify.kiosk_manager.application_logic.storage;

import me.factorify.kiosk_manager.data.entity.SavedFile;
import me.factorify.kiosk_manager.data.entity.Kiosk;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Interface for handling file storage.
 */
@Service
public interface StorageService {
    /**
     * Create SavedFile entity and save the file content.
     *
     * @param multipartFile file content
     * @param kiosk         nullable, good when saving system/browser logs
     * @param type          type of saved file see {@link SavedFile.Type}
     * @return SavedFile
     * @throws IOException if saving to disk fails
     */
    SavedFile save(MultipartFile multipartFile, Kiosk kiosk, SavedFile.Type type) throws IOException;

    /**
     * Create SavedFile entity and save the file content. Used for creating system logs.
     */
    default SavedFile saveSystemLog(MultipartFile multipartFile, Kiosk kiosk) throws IOException {
        return this.save(multipartFile, kiosk, SavedFile.Type.SYSTEM_LOG);
    }

    /**
     * Create SavedFile entity and save the file content. Used for creating browser logs.
     */
    default SavedFile saveBrowserLog(MultipartFile multipartFile, Kiosk kiosk) throws IOException {
        return this.save(multipartFile, kiosk, SavedFile.Type.BROWSER_LOG);
    }

    Optional<SavedFile> getById(long id);

    /**
     * Gets by id or throws {@link javax.persistence.EntityNotFoundException}
     *
     * @return SavedFile
     * @throws javax.persistence.EntityNotFoundException when entity is not found
     */
    SavedFile getByIdOrThrow(long id);

    /**
     * Gets the file content
     *
     * @return file content as byte array.
     * @throws IOException when reading the file fails
     */
    byte[] getFileContent(long id) throws IOException;

    /**
     * Gets and filters files based on kiosk name.
     * All parameters are optional. If all are absent, all files will be returned.
     */
    List<SavedFile> getFiltered(String kioskName, SavedFile.Type type);

    /**
     * Same as {@link #getFiltered(String, SavedFile.Type)} and marks the isSysLog and isBrowserLog parameters to be ignored.
     * Mainly used for readability of code where filtering is done only based on the kiosk name.
     */
    default List<SavedFile> getByKioskName(String kioskName) {
        return getFiltered(kioskName, null);
    }

    /**
     * Deletes file with ID both from disk and DB.
     * If deleting from disk fails, DB entry stays.
     * If file does not exist, then the DB entry is removed.
     */
    void delete(long id) throws IOException;
}
