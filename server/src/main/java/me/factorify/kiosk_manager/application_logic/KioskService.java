package me.factorify.kiosk_manager.application_logic;

import me.factorify.kiosk_manager.data.repository.KioskRepository;
import me.factorify.kiosk_manager.data.entity.Kiosk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

/**
 * Service for handling basic CRUD operations.
 */
@Service
public class KioskService {
    private final KioskRepository kioskRepository;

    @Autowired
    public KioskService(KioskRepository kioskRepository) {
        this.kioskRepository = kioskRepository;
    }


    public List<Kiosk> getAll() {
        return kioskRepository.getAll();
    }

    public Kiosk save(Kiosk kiosk) {
        Validate.kioskName(kiosk.getName());
        return kioskRepository.save(kiosk);
    }

    public Optional<Kiosk> getByName(String kioskName) {
        return kioskRepository.getByName(kioskName);
    }

    public Optional<Kiosk> getById(long id) {
        return this.kioskRepository.getById(id);
    }

    public List<Kiosk> getByIds(List<Long> ids) {
        return this.kioskRepository.getByIds(ids);
    }

    /**
     * Gets Kiosk by ID or throws {@link EntityNotFoundException} if not found.
     */
    public Kiosk getByIdOrThrow(long id) {
        return getById(id).orElseThrow(() -> new EntityNotFoundException("Kiosk with id " + id + " not found."));
    }

    /**
     * Gets Kiosk by NAME or throws {@link EntityNotFoundException} if not found.
     */
    public Kiosk getByNameOrThrow(String kioskName) {
        return getByName(kioskName).orElseThrow(EntityNotFoundException::new);
    }

    /**
     * Deletes kiosk with id. If not found no exception is thrown.
     */
    public void delete(long id) {
        kioskRepository.delete(id);
    }
}
