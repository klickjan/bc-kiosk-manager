package me.factorify.kiosk_manager.application_logic;

public class Validate {
    /**
     * Kiosk name can be non empty string consisting of [a-zA-Z0-9].
     */
    public static String kioskName(String kioskName) {
        if (!kioskName.matches("^[a-zA-Z0-9]+$")) {
            throw new IllegalArgumentException("Kiosk name " + kioskName + " is invalid.");
        }
        return kioskName;
    }
}
