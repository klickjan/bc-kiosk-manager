package me.factorify.kiosk_manager.application_logic.client_action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Class for representing actions to be done by the kiosk client.
 * If the action needs additional information (eg. URL), then the field parameters should be used.
 */
public class ClientAction {
    private final Type type;
    private final List<String> parameters;

    public ClientAction(Type type, List<String> parameters) {
        this.type = type;
        this.parameters = parameters;
    }

    public ClientAction(Type type, String... parameters) {
        this.type = type;
        this.parameters = Arrays.asList(parameters);
    }

    public Type getType() {
        return type;
    }

    public List<String> getParameters() {
        return parameters;
    }

    public enum Type {
        SYSLOG,
        BROWSER_LOG,
        RESTART,
        SSH_ADD,
        URL
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientAction that = (ClientAction) o;
        return type == that.type &&
                Objects.equals(parameters, that.parameters);
    }

    @Override
    public String toString() {
        return "ClientAction{" +
                "type=" + type +
                ", parameters=" + parameters +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, parameters);
    }
}
