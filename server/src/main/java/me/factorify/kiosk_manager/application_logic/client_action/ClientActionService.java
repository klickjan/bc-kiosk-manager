package me.factorify.kiosk_manager.application_logic.client_action;

import me.factorify.kiosk_manager.data.entity.ClientConnection;
import me.factorify.kiosk_manager.data.entity.Kiosk;

import java.util.List;
import java.util.Map;

/**
 * Interface for handling logic around kiosk client actions both for the client and the user, such as restarting, requesting logs, registering etc.
 * <p>
 * Finishing actions is not implemented - when a kiosk client requests pending actions, they will be sent to the client and removed from memory.
 * Possibly could be implemented by releasing events for finishing specified action on corresponding methods - eg. uploading syslog releases SyslogUploadedEvent -
 * and listening to these events and removing them only after the event has been released.
 */
public interface ClientActionService {
    /**
     * If specified kiosk is not registered within the system, new one will be registered with specified url (nullable). Otherwise does nothing.
     */
    Kiosk register(String kioskName, String url);

    /**
     * Gets all actions that are currently requested from all kiosk clients, does not modify state
     */
    Map<String, List<ClientAction>> getAllActions();

    /**
     * Gets actions that are currently requested from specified kiosk.
     */
    List<ClientAction> getActionsFor(Kiosk kiosk);

    /**
     * Gets and REMOVE actions that are currently requested from specified kiosk.
     * Always adds URL action to the top if URL is not null.
     */
    List<ClientAction> processActions(Kiosk kiosk);

    /**
     * Adds action for one kiosk.
     */
    void addAction(Kiosk kiosk, ClientAction action);

    /**
     * Adds action for a set of kiosks.
     */
    void addAction(List<Kiosk> kiosks, ClientAction clientAction);

    /**
     * Adds specified action for all kiosks.
     */
    void addAction(ClientAction action);

    /**
     * Registers connection from kiosk with IP address.
     */
    ClientConnection heartbeat(Kiosk kiosk, String ipAddress);

    /**
     * Removes all actions.
     */
    void clearActions();

    /**
     * Proxy for {@link #addAction(Kiosk, ClientAction)} that adds request for system log.
     */
    default void requestSystemLog(Kiosk kiosk) {
        addAction(kiosk, new ClientAction(ClientAction.Type.SYSLOG));
    }

    /**
     * Proxy for {@link #addAction(Kiosk, ClientAction)} that adds request for browser log.
     */
    default void requestBrowserLog(Kiosk kiosk) {
        addAction(kiosk, new ClientAction(ClientAction.Type.BROWSER_LOG));
    }

    /**
     * Proxy for {@link #addAction(Kiosk, ClientAction)} that adds request for kiosk restart.
     */
    default void requestRestart(Kiosk kiosk) {
        addAction(kiosk, new ClientAction(ClientAction.Type.RESTART));
    }

    /**
     * Proxy for {@link #addAction(Kiosk, ClientAction)} that adds request for adding public part of ssh key.
     */
    default void requestAddSSH(Kiosk kiosk, String sshPublicPart) {
        addAction(kiosk, new ClientAction(ClientAction.Type.SSH_ADD, sshPublicPart));
    }
}
