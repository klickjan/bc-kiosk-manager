package me.factorify.kiosk_manager.api.dto;

import me.factorify.kiosk_manager.data.entity.ClientConnection;
import me.factorify.kiosk_manager.data.entity.Kiosk;

import java.sql.Connection;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * DTO object about kiosk and its connections.
 */
public class KioskStatistics {
    private final Kiosk kiosk;
    private final List<ClientConnection> connections;

    private KioskStatistics(Kiosk kiosk, List<ClientConnection> connections) {
        this.kiosk = kiosk;
        this.connections = connections;
    }

    /**
     * Builds KioskStatistics with sorted client connections starting with the newest connection.
     */
    public static KioskStatistics build(Kiosk kiosk, List<ClientConnection> connections) {
        return new KioskStatistics(
                kiosk,
                connections.stream().sorted(Comparator.comparing(ClientConnection::getLastConnectedAt).reversed()).collect(Collectors.toList()));
    }

    public Kiosk getKiosk() {
        return kiosk;
    }

    public List<ClientConnection> getConnections() {
        return connections;
    }

    /**
     * Determines if kiosk was active within last hour.
     */
    public boolean isActive() {
        return this.isActive(LocalDateTime.now().minusHours(1));
    }

    /**
     * Determines whether kiosk is active or not based on input threshold.
     *
     * @param activeThreshold furthest datetime of connection that is considered active
     */
    private boolean isActive(LocalDateTime activeThreshold) {
        return !this.connections.isEmpty() && this.connections.get(0).getLastConnectedAt().isAfter(activeThreshold);
    }

    @Override
    public String toString() {
        return "KioskStatistics{" +
                "kiosk=" + kiosk +
                ", connections=" + connections +
                ", isActive=" + isActive() +
                '}';
    }
}
