package me.factorify.kiosk_manager.api.exception;

/**
 * Exception to signal that client sent bad request.
 */
public class BadRequestException extends RuntimeException {
    public BadRequestException(String s) {
        super(s);
    }
}
