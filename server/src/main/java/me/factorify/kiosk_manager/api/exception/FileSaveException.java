package me.factorify.kiosk_manager.api.exception;

/**
 * Exception that signals saving file failed.
 */
public class FileSaveException extends RuntimeException {
    public FileSaveException(Exception e) {
        super(e);
    }
}
