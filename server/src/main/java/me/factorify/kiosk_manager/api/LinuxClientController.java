package me.factorify.kiosk_manager.api;

import me.factorify.kiosk_manager.api.dto.RegisterKioskDTO;
import me.factorify.kiosk_manager.api.serializers.ClientActionTextSerializer;
import me.factorify.kiosk_manager.application_logic.KioskService;
import me.factorify.kiosk_manager.application_logic.client_action.ClientAction;
import me.factorify.kiosk_manager.application_logic.client_action.ClientActionService;
import me.factorify.kiosk_manager.data.entity.ClientConnection;
import me.factorify.kiosk_manager.data.entity.Kiosk;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


/**
 * Controller exposing interface for handling actions for the linux client.
 * If the request specifies a kiosk, that is not found, a {@link javax.persistence.EntityNotFoundException} will be thrown with code 404..
 */
@RestController
@RequestMapping(path = "client/linux")
public class LinuxClientController {
    private static final Logger log = LoggerFactory.getLogger(ClientActionController.class);
    private final KioskService kioskService;

    private final ClientActionService clientActionService;

    @Autowired
    public LinuxClientController(ClientActionService clientActionService, KioskService kioskService) {
        this.clientActionService = clientActionService;
        this.kioskService = kioskService;
    }

    /**
     * PUT "client/linux/register/{kioskName}"
     * If specified kiosk is not registered within the system, new one will be registered (together with supplied URL if present). Otherwise does nothing.
     * See {@link me.factorify.kiosk_manager.application_logic.Validate#kioskName(String)} for kiosk name constraints.
     */
    @PutMapping(path = "/register/{kioskName}")
    public Kiosk registerKiosk(@PathVariable String kioskName, @RequestBody RegisterKioskDTO request) {
        log.info("Registering kiosk {} with url {}", kioskName, request.getUrl());
        return clientActionService.register(
                kioskName,
                Strings.isNotEmpty(request.getUrl()) ? request.getUrl() : null
        );
    }

    /**
     * POST "client/linux/action/{kioskName}"
     * Gets and REMOVES actions that are currently requested from specified kiosk.
     * This endpoint also registers a connection made from the kiosk, which will create a new {@link ClientConnection}.
     * If kiosk has specified URL, then the first action will always be the URL.
     * If RESTART is included, it will be sent as last action.
     * <p>
     * Actions are serialized with {@link ClientActionTextSerializer} in following format
     * <p>
     * actionType;param1;param2;..
     * actionType2;param1;param2;..
     * ...
     * <p>
     * actionType is the string representation of enum value {@link ClientAction.Type}
     * if no parameter is specified, the leading ';' delimiter will be omitted
     * example:
     * URL;www.factorify.cz
     * SYS_LOG
     * RESTART
     */
    @PostMapping(path = "/action/{kioskName}", produces = {MediaType.TEXT_PLAIN_VALUE})
    public String pendingActions(@PathVariable(name = "kioskName") String kioskName, HttpServletRequest request) {
        log.info("Retrieving actions for kiosk {}", kioskName);
        var kiosk = kioskService.getByNameOrThrow(kioskName);
        clientActionService.heartbeat(kiosk, request.getRemoteAddr());
        return new ClientActionTextSerializer(
                clientActionService.processActions(kiosk)
        ).serialize();
    }
}
