package me.factorify.kiosk_manager.api.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

/**
 * Controller that defines response status when a server throws certain exception.
 */
@RestControllerAdvice
public class ControllerExceptionHandler {
    private static final Logger log = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    /**
     * Sends 400 BAD REQUEST when the client request is incorrect (BadRequestException).
     */
    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Bad request")
    public void badRequest(BadRequestException e) {
        log.error("BadRequestException handler.", e);
    }

    /**
     * Sends Sends 400 BAD REQUEST when the client request is incorrect (IllegalArgumentException).
     */
    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Bad request - illegal argument.")
    public void illegalArgumentException(IllegalArgumentException e) {
        log.error("IllegalArgumentException handler", e);
    }

    /**
     * Sends Sends 400 BAD REQUEST when path variable of type Long has different format (NumberFormatException).
     */
    @ExceptionHandler(NumberFormatException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Bad request - number format.")
    public void illegalArgumentException(NumberFormatException e) {
        log.error("NumberFormatException handler", e);
    }

    /**
     * Returns 404 NOT FOUND when an EntityNotFoundException is thrown.
     */
    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Entity not found")
    public void notFound(EntityNotFoundException e) {
        log.error("EntityNotFoundException handler.", e);
    }

    /**
     * Returns 409 CONFLICT when an EntityExistsException is thrown.
     */
    @ExceptionHandler(EntityExistsException.class)
    @ResponseStatus(value = HttpStatus.CONFLICT, reason = "Entity exists")
    public void alreadyExists(EntityExistsException e) {
        log.error("EntityExistsException handler.", e);
    }

    /**
     * Sends 500 INTERNAL SERVER ERROR when saving file fails (FileSaveException).
     */
    @ExceptionHandler(FileSaveException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "File save failed.")
    public void fileSaveException(FileSaveException e) {
        log.error("FileSaveException handler", e);
    }

    /**
     * Sends 500 INTERNAL SERVER ERROR when deleting file fails (FileDeleteException).
     */
    @ExceptionHandler(FileDeleteException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "File delete failed.")
    public void fileDeleteException(FileDeleteException e) {
        log.error("FileDeleteException handler", e);
    }
}
