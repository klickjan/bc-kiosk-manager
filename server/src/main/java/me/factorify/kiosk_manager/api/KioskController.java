package me.factorify.kiosk_manager.api;

import me.factorify.kiosk_manager.api.dto.KioskStatistics;
import me.factorify.kiosk_manager.api.exception.BadRequestException;
import me.factorify.kiosk_manager.application_logic.KioskService;
import me.factorify.kiosk_manager.data.entity.Kiosk;
import me.factorify.kiosk_manager.data.repository.ClientConnectionsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller for exposing CRUD operations over the kiosk entity.
 * <p>
 * If the request specifies a kiosk, that is not found, a {@link javax.persistence.EntityNotFoundException} will be thrown with code 404..
 */
@RestController
@RequestMapping(path = "kiosk")
public class KioskController {
    private static final Logger log = LoggerFactory.getLogger(KioskController.class);

    private final KioskService kioskService;

    private final ClientConnectionsRepository clientConnectionsRepository;

    @Autowired
    public KioskController(KioskService kioskService, ClientConnectionsRepository clientConnectionsRepository) {
        this.kioskService = kioskService;
        this.clientConnectionsRepository = clientConnectionsRepository;
    }

    /**
     * GET "kiosk"
     * Returns all kiosks.
     */
    @GetMapping(path = "")
    public List<Kiosk> getAll() {
        log.info("Getting all kiosks.");
        return kioskService.getAll();
    }

    /**
     * GET "kiosk/{kioskId}"
     */
    @GetMapping(path = "/{kioskId}")
    public Kiosk getById(@PathVariable("kioskId") long id) {
        log.info("Getting kiosk {}", id);
        return kioskService.getByIdOrThrow(id);
    }

    /**
     * POST "kiosk"
     * Creates kiosk. Not used for updating kiosks.
     * See {@link me.factorify.kiosk_manager.application_logic.Validate#kioskName(String)} for kiosk name constraints.
     *
     * @return newly created kiosk with status 201
     * @throws BadRequestException   if kiosk in the body contains ID.
     * @throws EntityExistsException if kiosk with specified name exists
     */
    @PostMapping(path = "")
    public ResponseEntity<Kiosk> create(@RequestBody Kiosk kiosk) {
        log.info("Creating kiosk {}", kiosk);
        if (kiosk.getId() != null) {
            log.error("Posted kiosk contains ID, use PUT instead.");
            throw new BadRequestException("Posted kiosk contains ID. Use PUT instead.");
        } else if (kioskService.getByName(kiosk.getName()).isPresent()) {
            log.error("Kiosk to be created already exists. Use PUT instead.");
            throw new EntityExistsException("Kiosk to be created already exists. Use PUT instead.");
        } else {
            var entity = kioskService.save(kiosk);
            return ResponseEntity.status(HttpStatus.CREATED).body(entity);
        }
    }

    /**
     * PUT "kiosk/{id}"
     * Updates specified kiosk. If any field is missing, it will be considered as null.
     * See {@link me.factorify.kiosk_manager.application_logic.Validate#kioskName(String)} for kiosk name constraints.
     *
     * @return updated kiosk
     */
    @PutMapping(path = "/{id}")
    public ResponseEntity<Kiosk> update(@PathVariable long id, @RequestBody Kiosk kiosk) {
        log.info("PUT id {} for kiosk {}", id, kiosk);
        var entity = kioskService.getById(id).orElseThrow(EntityNotFoundException::new);
        return ResponseEntity.status(HttpStatus.OK)
                .body(kioskService.save(entity.copyValuesFrom(kiosk)));
    }

    /**
     * DELETE "kiosk/{id}"
     * Deletes kiosk by id.
     */
    @DeleteMapping(path = "/{id}")
    public void delete(@PathVariable long id) {
        log.info("Deleting kiosk {}", id);
        kioskService.getByIdOrThrow(id);
        kioskService.delete(id);
    }

    /**
     * GET "kiosk/stats"
     * Returns {@link KioskStatistics} about all kiosks and their connections to the server.
     * TODO optimize
     */
    @GetMapping(path = "/stats")
    public List<KioskStatistics> statistics() {
        log.info("Generating statistics for all kiosks.");
        return kioskService.getAll().stream()
                .map(kiosk -> KioskStatistics.build(kiosk, clientConnectionsRepository.findByKioskId(kiosk.getId())))
                .collect(Collectors.toList());
    }

    /**
     * GET "kiosk/{id}/stats"
     * Returns info about kiosk and its connections to the server.
     */
    @GetMapping(path = "/{id}/stats")
    public KioskStatistics statistics(@PathVariable long id) {
        log.info("Generating statistics for kiosk {}", id);
        return KioskStatistics.build(kioskService.getByIdOrThrow(id), clientConnectionsRepository.findByKioskId(id));
    }
}
