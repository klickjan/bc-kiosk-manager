package me.factorify.kiosk_manager.api;

import me.factorify.kiosk_manager.api.exception.FileDeleteException;
import me.factorify.kiosk_manager.api.exception.FileSaveException;
import me.factorify.kiosk_manager.application_logic.KioskService;
import me.factorify.kiosk_manager.application_logic.storage.StorageService;
import me.factorify.kiosk_manager.data.entity.SavedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * Controller for handling file storage.
 * <p>
 * If file is not found {@link javax.persistence.EntityNotFoundException} will be thrown with code 404.
 */
@RestController
@RequestMapping("storage")
public class StorageController {
    private static final Logger log = LoggerFactory.getLogger(StorageController.class);

    private final StorageService storageService;

    private final KioskService kioskService;

    @Autowired
    public StorageController(StorageService storageService, KioskService kioskService) {
        this.storageService = storageService;
        this.kioskService = kioskService;
    }

    /**
     * GET "storage", query parameters kiosk_name, file_type
     * Gets and filters saved_file entity. To request file content, use the {@link #getFileContent(long)} method.
     * <p>
     * All parameters are optional. If all are absent, all files will be returned.
     */
    @GetMapping(path = "")
    public List<SavedFile> getFiltered(@RequestParam(name = "kiosk_name", required = false) String kioskName,
                                       @RequestParam(name = "file_type", required = false) SavedFile.Type fileType) {
        log.info("Requesting files with filters kiosk {}, file type {}", kioskName, fileType);
        return storageService.getFiltered(kioskName, fileType);
    }

    /**
     * GET "storage/{id}"
     * Gets SavedFile entity by ID
     */
    @GetMapping(path = "/{id}")
    public SavedFile getById(@PathVariable("id") Long id) {
        log.info("Requesting file with ID {}", id);
        return storageService.getByIdOrThrow(id);
    }

    /**
     * GET "storage/{id}/content"
     * Gets the file content by ID
     */
    @GetMapping(value = "/{id}/content")
    public FileSystemResource getFileContent(@PathVariable("id") long id) {
        return new FileSystemResource(storageService.getByIdOrThrow(id).getPath());
    }

    /**
     * POST "storage/log/browser/{kioskName}"
     * Uploads BROWSER log for kiosk. Used mainly by the client.
     */
    @PostMapping(path = "/log/browser/{kioskName}")
    public SavedFile uploadBrowserLog(@PathVariable("kioskName") String kioskName, @RequestParam("file") MultipartFile file) {
        log.info("Uploading browser log {} from kiosk {}", file.getOriginalFilename(), kioskName);
        try {
            return storageService.saveBrowserLog(file, kioskService.getByNameOrThrow(kioskName));
        } catch (IOException e) {
            log.error("Failed saving log.", e);
            throw new FileSaveException(e);
        }
    }

    /**
     * POST "storage/log/system/{kioskName}"
     * Uploads SYSTEM log for kiosk. Used mainly by the client.
     */
    @PostMapping(path = "/log/system/{kioskName}")
    public SavedFile uploadSystemLog(@PathVariable("kioskName") String kioskName, @RequestParam("file") MultipartFile file) {
        log.info("Uploading system log {} from kiosk {}", file.getOriginalFilename(), kioskName);
        try {
            return storageService.saveSystemLog(file, kioskService.getByNameOrThrow(kioskName));
        } catch (IOException e) {
            log.error("Failed saving log.", e);
            throw new FileSaveException(e);
        }
    }

    /**
     * DELETE "storage/{id}"
     * Deletes file with ID both from disk and DB.
     * If deleting from disk fails, DB entry stays.
     * If file does not exist, then the DB entry is removed.
     */
    @DeleteMapping(path = "/{id}")
    public void delete(@PathVariable long id) {
        log.info("Deleting saved file {}", id);
        try {
            storageService.delete(id);
        } catch (IOException e) {
            log.error("Failed saving log.", e);
            throw new FileDeleteException(e);
        }
    }
}
