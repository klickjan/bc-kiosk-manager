package me.factorify.kiosk_manager.api.dto;

public class RegisterKioskDTO {
    private String kioskName;
    private String url;

    public String getKioskName() {
        return kioskName;
    }

    public void setKioskName(String kioskName) {
        this.kioskName = kioskName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
