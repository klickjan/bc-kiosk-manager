package me.factorify.kiosk_manager.api.dto;

import java.util.List;

/**
 * DTO for requesting saving SSH keys to kiosks.
 */
public class SaveSSHKeyDTO {
    /**
     * Kiosk ids. Can be empty if {@link #forAllKiosks} is true.
     */
    private List<Long> kioskIds;
    /**
     * The public part of SSH key to add.
     */
    private String sshPublicPart;
    /**
     * Can be set to true to add this SSH key to all kiosks.
     */
    private boolean forAllKiosks;

    public SaveSSHKeyDTO() {
        this.kioskIds = List.of();
        this.sshPublicPart = null;
        this.forAllKiosks = false;
    }

    public SaveSSHKeyDTO(String sshPublicPart, List<Long> kioskIds) {
        this.kioskIds = kioskIds;
        this.sshPublicPart = sshPublicPart;
        this.forAllKiosks = false;
    }

    public SaveSSHKeyDTO(String sshPublicPart, boolean forAllKiosks) {
        this.kioskIds = List.of();
        this.sshPublicPart = sshPublicPart;
        this.forAllKiosks = forAllKiosks;
    }

    public List<Long> getKioskIds() {
        return kioskIds;
    }

    public void setKioskIds(List<Long> kioskIds) {
        this.kioskIds = kioskIds;
    }

    public String getSshPublicPart() {
        return sshPublicPart;
    }

    public void setSshPublicPart(String sshPublicPart) {
        this.sshPublicPart = sshPublicPart;
    }

    public boolean isForAllKiosks() {
        return forAllKiosks;
    }

    public void setForAllKiosks(boolean forAllKiosks) {
        this.forAllKiosks = forAllKiosks;
    }

    @Override
    public String toString() {
        return "UploadSSHKeyDTO{" +
                "kioskIds=" + kioskIds +
//                ", sshPublicPart='" + sshPublicPart + '\'' +
                ", forAllKiosks=" + forAllKiosks +
                '}';
    }
}
