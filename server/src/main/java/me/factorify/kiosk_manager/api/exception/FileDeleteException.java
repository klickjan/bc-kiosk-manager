package me.factorify.kiosk_manager.api.exception;

/**
 * Exception that signals deleting file failed.
 */
public class FileDeleteException extends RuntimeException {
    public FileDeleteException(Exception e) {
        super(e);
    }
}
